export const environment = {
  firebase: {
    projectId: 'ema-uebung-31ad0',
    appId: '1:240010515116:web:904fcce733279b0e66c509',
    databaseURL: 'https://ema-uebung-31ad0-default-rtdb.europe-west1.firebasedatabase.app',
    storageBucket: 'ema-uebung-31ad0.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyDpCV-N5vtLIAvGzgneQ0YryBsZdhXdiqo',
    authDomain: 'ema-uebung-31ad0.firebaseapp.com',
    messagingSenderId: '240010515116',
    measurementId: 'G-M60DZ3M7YC',
  },
  production: true,

  firebaseConfig: {
    apiKey: 'AIzaSyDpCV-N5vtLIAvGzgneQ0YryBsZdhXdiqo',
    authDomain: 'ema-uebung-31ad0.firebaseapp.com',
    projectId: 'ema-uebung-31ad0',
    storageBucket: 'ema-uebung-31ad0.appspot.com',
    messagingSenderId: '240010515116',
    appId: '1:240010515116:web:904fcce733279b0e66c509',
    measurementId: 'G-M60DZ3M7YC'
  }
};
