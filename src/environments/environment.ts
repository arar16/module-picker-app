// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  /*firebase: {
    projectId: 'ema-uebung-31ad0',
    appId: '1:240010515116:web:904fcce733279b0e66c509',
    databaseURL: 'https://ema-uebung-31ad0-default-rtdb.europe-west1.firebasedatabase.app',
    storageBucket: 'ema-uebung-31ad0.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyDpCV-N5vtLIAvGzgneQ0YryBsZdhXdiqo',
    authDomain: 'ema-uebung-31ad0.firebaseapp.com',
    messagingSenderId: '240010515116',
    measurementId: 'G-M60DZ3M7YC',
  },*/
  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyDpCV-N5vtLIAvGzgneQ0YryBsZdhXdiqo',
    authDomain: 'ema-uebung-31ad0.firebaseapp.com',
    projectId: 'ema-uebung-31ad0',
    storageBucket: 'ema-uebung-31ad0.appspot.com',
    messagingSenderId: '240010515116',
    appId: '1:240010515116:web:904fcce733279b0e66c509',
    measurementId: 'G-M60DZ3M7YC'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
