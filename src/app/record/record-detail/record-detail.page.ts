import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IonInput, NavController} from '@ionic/angular';
import {RecordService} from '../record.service';
import {Record} from '../../models/record.model';
import {ModulePickerPage} from '../../module/module-picker/module-picker.page';
import {ModalController} from '@ionic/angular';


@Component({
  selector: 'app-record-detail',
  templateUrl: './record-detail.page.html',
  styleUrls: ['./record-detail.page.scss'],
})
export class RecordDetailPage {

  @ViewChild('moduleNr') private moduleNrRef: IonInput;
  @ViewChild('moduleName') private moduleNameRef: IonInput;
  @ViewChild('CrP') private crPRef: IonInput;

  isEditMode = false;
  pageTitle: string;
  record = new Record();
  years: number[] = [];

  errors: Map<string, string> = new Map<string, string>();

  constructor(private route: ActivatedRoute,
              private recordService: RecordService,
              private navCtrl: NavController,
              private modalController: ModalController) {
    this.initYears();
  }

  async selectModule() {
    const modal = await this.modalController.create({
      component: ModulePickerPage
    });
    await modal.present();
    const result = await modal.onDidDismiss();
    this.record.moduleName = result.data.name;
    this.record.moduleNr = result.data.nr;
    this.record.crp = result.data.crp;
  }

  initYears(): void{
    for (let i = 2022; i > 1999; i--) {
      this.years.push(i);
    }
  }

  async ionViewWillEnter(){
    const recordId = this.route.snapshot.paramMap.get('id');
    if (recordId) {
      this.isEditMode = true;
      const r = await this.recordService.findById(recordId);
      Object.assign(this.record, r);
      this.pageTitle = 'Leistung bearbeiten';
    } else {
      this.record.year = new Date().getFullYear();
      this.pageTitle = 'Leistung erstellen';
      this.selectModule();
    }
  }

  ionViewDidEnter(){
    this.moduleNrRef.setFocus();
  }

  save() {
    this.errors.clear();
    if (!this.record.moduleNr) {
      this.errors.set('moduleNr', 'Modulnummer darf nicht leer sein!');
    }

    if(!this.record.crp) {
      this.errors.set('CrP', 'Creditpoints darf nicht leer sein');
    }

    if(!this.record.moduleName) {
      this.errors.set('ModuleName', 'ModuleName darf nicht leer sein');
    }

    if(this.record.grade < 50 || this.record.grade > 100){
      this.errors.set('grade', 'Note darf nur zwischen 50 und 100 liegen');
    }

    if(this.record.crp < 3 || this.record.crp > 15){
      this.errors.set('CrP', 'Creditpoints müssen zwischen 3 und 15 liegen');
    }

    if (this.errors.size === 0) {
      if (this.isEditMode) {
        this.recordService.update(this.record);
      } else {
        this.recordService.persist(this.record);
      }
      this.navCtrl.pop();
    }
  }

  deleteRecord(){
    this.recordService.delete(this.record.id);
    this.navCtrl.pop();
  }
}
