import {Injectable, OnDestroy} from '@angular/core';
import {Record} from '../models/record.model';
import {
  addDoc,
  collection,
  CollectionReference, deleteDoc,
  doc,
  DocumentData,
  Firestore,
  getDoc, getDocs,
  onSnapshot, updateDoc
} from '@angular/fire/firestore';
import firebase from 'firebase/compat';
import Unsubscribe = firebase.Unsubscribe;

@Injectable({
  providedIn: 'root'
})

export class RecordService implements OnDestroy{
  recordsCollectionRef: CollectionReference<DocumentData>;
  recordConverter = {
    fromFirestore: (snapshot, options): Record => {
      const result = Object.assign(new Record(), snapshot.data(options));
      result.id = snapshot.id;
      return result;
    },
    toFirestore: (record): DocumentData => {
      const copy = {...record};
      delete copy.id;
      copy.grade = copy.grade || null;
      return copy;
    }
  };
  private records: Record[];
  private subscription: Unsubscribe;


  constructor(private fireStore: Firestore) {
    this.recordsCollectionRef = collection(fireStore, 'records');
  }

  ionViewWillEnter(){
    const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
    this.subscription = onSnapshot(refWithConverter, snapshot => {
      const temp = [];
      snapshot.docs.forEach(docData => {
        temp.push(docData.data());
      });
      Object.assign(this.records, temp);
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription();
      this.subscription = null;
    }
  }

  async persist(record: Record): Promise<void> {
    const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
    const rec = await addDoc(refWithConverter, record);
    record.id = rec.id;
  }

  async findAll(): Promise<Record[]> {
    const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
    const recordDocs = await getDocs(refWithConverter);
    const temp = [];
    recordDocs.forEach(recordDoc => {
      temp.push(recordDoc.data());
    });
    return temp;
  }

  async findById(id: string): Promise<Record> {
    const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
    const docRef = doc(refWithConverter, id);
    const recordDoc = await getDoc(docRef);
    return recordDoc.data();
  }


  update(record: Record): boolean{
    try {
      const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
      const docRef = doc(refWithConverter, record.id);
      updateDoc(docRef, {
        crp: record.crp, moduleNr: record.moduleNr, moduleName: record.moduleName,
        grade: record.grade, halfWeighted: record.halfWeighted, summerTerm: record.summerTerm, year: record.year
       }).then();
      return true;
    }catch (Exception){
      return false;
    }
  }

  delete(id: string): boolean{
    try {
      const refWithConverter = this.recordsCollectionRef.withConverter(this.recordConverter);
      const docRef = doc(refWithConverter, id);
      deleteDoc(docRef).then();
      return true;
    }catch (Exception){
      return false;
    }
  }
}
