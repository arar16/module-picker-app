import {Component} from '@angular/core';
import {Record} from '../../models/record.model';
import {Router} from '@angular/router';
import {AlertController, IonSearchbar} from '@ionic/angular';
import {Statistic} from '../../models/statistic.model';
import {RecordService} from '../record.service';
import {Share} from '@capacitor/share';


@Component({
  selector: 'app-record-list',
  templateUrl: './record-list.page.html',
  styleUrls: ['./record-list.page.scss'],
})
export class RecordListPage  {

  records: Record[] = [];
  searchbarVisible = false;
  showTitle = true;
  #searchbar: IonSearchbar;
  filterTerm: string;
  statistic = new Statistic(this.records);

  // id, moduleNr, moduleName, crp, grade, halfWeighted, summerTerm, year
  constructor(private router: Router, public alert: AlertController, private recordService: RecordService) {
  }

  updateStats(): void{
    this.statistic = new Statistic(this.records);
  }

  sortRecords(): void{
    this.records = this.records.sort((a, b) => {
      if(a.moduleName < b.moduleName) { return -1; }
      if(a.moduleName > b.moduleName) { return 1; }
      return 0;});
  }

  async ionViewWillEnter() {
    await this.updateRecords();
    this.updateStats();
    this.sortRecords();
  }

  async updateRecords(): Promise<void>{
    this.records = await this.recordService.findAll();
  }

  createRecord(): void {
    this.router.navigate(['record-detail']);
  }
  showStats(): void {
    this.statistic = new Statistic(this.records);
    this.alert.create({
      header: 'Statistik',
      message: '<p>Anzahl Module: ' +  this.statistic.recordCount +'</p>' +
      '<p>50%-Leistungen: ' + this.statistic.hwCount + '</p>' +
        '<p>Summe CrP:  ' +  this.statistic.sumCrp + '</p>' +
        '<p>CrP bis Ziel: ' +  this.statistic.crpToEnd + '</p>' +
        '<p>Durchschnitt: : ' +  this.statistic.averageGrade + '%</p>',
      buttons: ['Schließen']
    }).then(res => res.present());
  }
  editRecord(record) {
    this.router.navigate(['record-detail', {id: record.id}]);
  }

  deleteRecord(record){
    this.alert.create(
      {
        header: 'Löschen bestätigen',
        message: 'Möchten sie dieses Element wirklich löschen',
        buttons:[
          {
            text: 'Nein',
            handler: () => this.alert.dismiss('test'),
          },
          {
            text: 'Ja',
            handler: () => {
              this.recordService.delete(record.id);
              this.updateRecords();
            }
          }
        ]
      }
    ).then(res => {
      res.present();
    });
  }

  shareRecords() {
    let msgText = 'Hallo, hier sind die aktuellen Ergebnisse meiner Module: \n';
    for (const item of this.records) {
      msgText += item.moduleName + ' ' + item.grade + '%\n';
    }
    console.log(msgText);
    Share.canShare().then(canShare => {
     if (canShare.value) {
       Share.share({
         title: 'Meine Studienleistungen',
         text: msgText,
         dialogTitle: 'Leistungen teilen'
       }).then((v) => console.log('ok: ', v))
         .catch(err => console.log(err));
     } else {
       console.log('Error: Sharing not available!');
     }
    });
  }

  showSearch() {
    this.searchbarVisible = true;
    this.showTitle = false;
    this.#searchbar?.setFocus();
  }

  cancelSearch() {
    this.searchbarVisible = false;
    this.showTitle = true;
    this.filterTerm = null;
  }
}
