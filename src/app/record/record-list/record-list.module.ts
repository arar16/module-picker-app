import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RecordListPageRoutingModule } from './record-list-routing.module';

import { RecordListPage } from './record-list.page';
import {Ng2SearchPipeModule} from "ng2-search-filter";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RecordListPageRoutingModule,
        Ng2SearchPipeModule
    ],
  declarations: [RecordListPage]
})
export class RecordListPageModule {}
