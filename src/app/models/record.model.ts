export class Record{

  constructor(public id?: string,
              public moduleNr?: string,
              public moduleName?: string,
              public crp?: number,
              public grade?: number,
              public halfWeighted: boolean = false,
              public summerTerm: boolean = false,
              public year?: number)
  {}

}
