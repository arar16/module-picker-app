import {Record} from './record.model';

export class Statistic{
  input: Record[];
  recordCount: number;
  hwCount: number;
  sumCrp: number;
  crpToEnd: number;
  averageGrade: number;

  constructor(input: Record[]) {
    this.input = input;
    this.recordCount = input.length;
    this.hwCount = input.filter(n => n.halfWeighted === true).length;
    this.sumCrp =  input.reduce((a, b) => a + b.crp, 0);
    this.crpToEnd = input.reduce((a, b) => a - b.crp, 180);
    this.averageGrade = Math.round(((input.filter(n => n.halfWeighted === true).reduce((a,b) => a + (b.grade * (b.crp/2) ), 0))
        + (input.filter(n => n.halfWeighted === false).reduce((a,b) => a + (b.grade * b.crp), 0)))
      / ((input.filter(n => n.halfWeighted === true).reduce((a,b) => a + (b.crp/2), 0)) +
        input.filter(n => n.halfWeighted === false).reduce((a,b) => a + b.crp, 0))) || 0;
  }
}
