import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModulePickerPageRoutingModule } from './module-picker-routing.module';

import { ModulePickerPage } from './module-picker.page';
import {Ng2SearchPipeModule} from "ng2-search-filter";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ModulePickerPageRoutingModule,
        Ng2SearchPipeModule
    ],
  declarations: [ModulePickerPage]
})
export class ModulePickerPageModule {}
