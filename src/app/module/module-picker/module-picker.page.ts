import {Component, ViewChild} from '@angular/core';
import {Module} from '../module.model';
import {IonSearchbar, ModalController} from '@ionic/angular';
import {ModuleService} from '../module.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-module-picker',
  templateUrl: './module-picker.page.html',
  styleUrls: ['./module-picker.page.scss'],
})
export class ModulePickerPage {

  modules: Module[] = [];
  filteredModules: Module[] = [];
  searchbarVisible = false;
  #searchbar: IonSearchbar;

  constructor(public modalController: ModalController, private moduleService: ModuleService, public navCtrl: NavController) {
    this.modules = moduleService.findAll();
    this.filteredModules = this.modules;
    this.moduleService.load();
    this.searchbarVisible = true;
  }

  @ViewChild(IonSearchbar)
  set searchbar(sb: IonSearchbar) {
    if (sb) {
      sb.setFocus().then();
      this.#searchbar = sb;
    }
  }
    doSearch() {
     this.filteredModules = this.modules.filter(m => m.name.toLowerCase().includes(this.#searchbar.value.toLowerCase()) ||
       m.nr.toLowerCase().includes(this.#searchbar.value.toLowerCase()));
    }
    cancelSearch() {
    this.modalController.dismiss(new Module(null, null, null)).then();
  }
}
