import { Injectable } from '@angular/core';
import {Module} from './module.model';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  static corsProxy = 'https://cors-anywhere.herokuapp.com/';
  static modulesurl = ModuleService.corsProxy + 'https://ema-data.firebaseio.com/modules.json';
  modules: Module[];
  constructor(private http: HttpClient) {
    this.load();
    const modulesJSON = localStorage.getItem('modules');
    if (modulesJSON) {
      this.modules = JSON.parse(modulesJSON);
    } else {
      // init storage with test data
      this.modules = [];
      // nr, name, crp
      this.modules.push(new Module('CS1019', 'Compilerbau', 6));
      this.save();
    }
    console.log(modulesJSON);
  }

  load() {
    const now = new Date();
    const temp = localStorage.getItem('lastRequest');
    const then = new Date(temp);
    const msBetweenDates = Math.abs(then.getTime() - now.getTime());
    const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);
    if(hoursBetweenDates > 24){
      this.requestUpdate();
      const modulesLastModified = localStorage.getItem('modulesLastModified');
      this.http.get<Module[]>(ModuleService.modulesurl, {
        observe: 'response',
        headers: modulesLastModified ? {'if-Modified-Since': modulesLastModified} : {}
      }).toPromise().then(
        (response: HttpResponse<Module[]>) => {
          const newModules = response.body;
          this.modules.splice(0, this.modules.length, ...newModules);
          localStorage.setItem('modulesLastModified', response.headers.get('Last-Modified'));
          this.save();
        }
        ,
        (error: HttpErrorResponse) => {
          // error.status == 0 No network, SOP, etc.
          // error.status == 304 modules.json not modified
        }
      );
    }
  }

  requestUpdate(): void{
    localStorage.setItem('lastRequest', new Date().toString());
  }

  findAll(): Module[] {
    return this.modules;
  }
  private save(): void {
    localStorage.setItem('modules', JSON.stringify(this.modules));
  }
}
