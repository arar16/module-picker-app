import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ema-arar16-u1',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
