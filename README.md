# Module Picker App

Perfect App for Universitiy Students to have a summary about their Modules

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.thm.de/arar16/module-picker-app.git
git branch -M master
git push -uf origin master
```

## Description
The Module Picker app is an excellent app to structure and save your achievements in your studies in an organized way.
On the start page you can see all completed modules.
At the top of the search bar you can search for individual modules.
You can also share your modules by simply pressing the share button next to the search button.

## Create new Module Entries
Just click the plus button at the bottom right and you will be redirected to a ModulePicker page, where ready-made modules are provided by the server. If none of them apply to yours, you can simply skip this and create your entry yourself.

## Visuals
![MainPage](/MainPage.jpg)
![MainPage Light](/Main%20Light.jpg)
![ModulePicker](/ModulePicker.jpg)
![RecordDetail](/RecordDetail.jpg)

